from library.api_calls import ApiCalls
import time
import csv

def main():
    # Authentication
    instance = 'https://<instance>.atlassian.net/'
    username = '<e-mail>'
    password = '<api_token>'

    # Set Authentication
    api_call = ApiCalls()
    api_call.setAuth('basic', username=username, password=password)

    group_name = '<group_name>'

    # Open the .CSV file with the users id and email
    with open('groups.csv', mode='r') as infile:
        reader = csv.reader(infile)
        mydict = {rows[0]: rows[1] for rows in reader}

    for id, email in mydict.items():
        # Print a confirmation of which user it is trying to remove
        print("Processing addition of id: " + id + " with email: " + email)
        user_data = {
            'accountId': id
        }
        response = api_call.request('POST', instance + '/rest/api/3/group/user?groupname=' + group_name, json_data=user_data)
        print(response.status_code)

if __name__ == '__main__':
    main()
