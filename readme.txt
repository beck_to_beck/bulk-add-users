Intructions:

1. Authentication
* In line 7 replace '<instance>' with the name of the isntance you will add users to a group;
* In line 9, replace '<e-mail>' with your e-mail;
* In line 10, replace '<api_token>' with your API token;

2. Group Name
* In line 15, replace '<group-name>' with the group you want to add users to;

3. Columns Mapping
In line 20 it's important to map the columns with account id and e-mail of the user;
Refer to the example file named 'group_list_example.csv' to check if the file you have follows the same pattern. Note, the list must contain a column with the users' account id and e-mail. Other information is optional and will not be necessary. The file musn't have a header row.
For example, if your list is similar to:
accountid1, email1
accountid2, email2

You should re-write line 20 as:
mydict = {rows[0]: rows[1] for rows in reader}

If your list is similar to:
accountid1, user1, email1
accountid2, user2, email2

You should re-write line 20 as:
mydict = {rows[0]: rows[2] for rows in reader}

The first column will have index starting at 0.

4. Logs
At the end of the file execution, you'll have two lines printed by user, one containing information associated with the user e-mail and account id, and the other with the status of the request. If you want to keep that informatioin, remember to copy the output of this from Terminal.